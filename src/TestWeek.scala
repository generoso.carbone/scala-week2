/**
  * Created by generoso on 07/03/17.
  */

import SecondWeek._
import Currying._
import Rationals._

object TestWeek{
  def main(args: Array[String]) {
    println("higher-order functions")
    println(sumInt(3, 4))
    println(sumCube(3, 4))
    println(sumFact(3, 4))

    println("anonymous functions")
    println(anSumInt(3, 4))
    println(anSumCube(3, 4))

    println("tail recursion with anonymous functions")
    println(sumTR(x=> x)(3, 4))
    println(sumTR(x=> x*x*x)(3, 4))

    println("Currying")
    println(curryingSum(id)(3, 4))
    println(curryingSum(cube)(3, 4))
    println(curryingSum(fact)(3, 4))

    println("Product")
    println(product(x=>x*x)(3,4))
    println(factorial(5))
    println(gProduct(x=>x*x)(3,4))

    val r = new Rational(1, 2)
    println(r.numer)
    println(r.denom)

    println(makeStr(addRational(new Rational(1, 2), new Rational(2, 3))))

    val x = new Rational(1, 3)
    val y = new Rational(5, 7)
    val z = new Rational(3, 2)

    println(x - y - z)
    println(y + y )

    //infix notation
    //x add y
    //instead of
    //x.add(y)
  }
}