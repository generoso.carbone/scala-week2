/**
  * Created by generoso on 07/03/17.
  * Higher-Order functions: function that acts on other functions
  * Anonymous-functions: functions that have no name
  */
object SecondWeek {

  /**
    * Higher-Order Functions
    */
  def id(x: Int): Int = x

  def cube(x: Int): Int = x * x * x

  def fact(x: Int): Int = if(x==0) 1 else fact(x-1)

  private def sum(f: Int => Int, a: Int, b: Int): Int =
    if(a > b) 0
    else f(a) + sum(f, a+1, b)

  def sumInt(a: Int, b: Int) = sum(id, a, b)

  def sumCube(a: Int, b: Int) = sum(cube, a, b)

  def sumFact(a: Int, b: Int) = sum(fact, a, b)


  /**
    * Anonymous High-Order functions
    */
  //sum((x: Int) => x, a, b)
  def anSumInt(a: Int, b: Int) = sum(x => x, a, b)

  def anSumCube(a: Int, b: Int) = sum((x: Int) => x * x * x, a, b)

  def sumTR(f: Int => Int)(a: Int, b: Int): Int = {
    def loop(a: Int, acc: Int): Int = {
      if (a>b) acc
      else loop(a+1, f(a) + acc)
    }
    loop(a, 0)
  }
}
