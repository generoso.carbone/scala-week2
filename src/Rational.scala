/**
  * Created by generoso on 10/03/17.
  */
class Rational(x: Int, y: Int) {

  require(y > 0, "denominator must be positive")

  private val g = gcd(x, y)

  def numer = x
  def denom = y

  //another constructor
  def this(x: Int) = this(x, 1)

  def  + (that: Rational) = {
    new Rational(numer * that.denom + denom * that.numer, denom * that.denom)
  }

  def - (that: Rational) = {
    this + -that
  }

  def *(that: Rational) = {
    new Rational(numer * that.numer, denom * that.denom)
  }

  def /(that: Rational) = {
    new Rational(numer * that.denom, denom * that.numer)
  }

  override def toString = {
    val g = gcd(numer, denom)
    numer/g + "/" + denom/g
  }

  def unary_- : Rational = new Rational(-numer, denom)

  private def gcd(a: Int, b: Int): Int = {
    if(b==0) a
    else gcd(b, a % b)
  }

  //def less(that: Rational) = numer * that.denom < denom * that.numer
  def <(that: Rational) = numer * that.denom < denom * that.numer

  def max(that: Rational) =
    //if(this.less(that)) that else this
    if(this < that) that else this
}

object Rationals{
  def addRational(r: Rational, s: Rational): Rational = {
    new Rational(r.numer * s.denom + r.denom * s.numer, r.denom * s.denom)
  }

  def subtractRational(r: Rational, s: Rational): Rational = {
    new Rational(r.numer * s.denom - r.denom * s.numer, r.denom * s.denom)
  }

  def multiplyRational(r: Rational, s: Rational): Rational = {
    new Rational(r.numer * s.numer, r.denom * s.denom)
  }

  def divideRational(r: Rational, s: Rational): Rational = {
    new Rational(r.numer * s.denom, r.denom * s.numer)
  }

  def makeStr(r: Rational) = {
    r.numer + "/" + r.denom
  }
}