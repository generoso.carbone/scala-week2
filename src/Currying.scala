/**
  * Created by generoso on 07/03/17.
  * Currying
  */
object Currying {

  /*
   * Function that returns a function
   */
  def curryingSum(f: Int => Int): (Int, Int) => Int = {
    def sum(a: Int, b: Int): Int =
      if(a>b) 0
      else f(a) + sum(a+1, b)
    sum
  }

  /*
   * Write a "product" function that calculates the product of the values of a function
   * for the points of a given interval
   */
  def product(f: Int => Int)(a: Int, b: Int): Int = {
    if (a > b) 1
    else f(a) * product(f)(a + 1, b)
  }

  /*
   * Write factorial in terms of product
   */
  def factorial(a: Int): Int = {
    product(x=>x)(1, a)
  }

  /*
   * Write a function that generalizes both sum and product
   */
  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int): Int = {
      if (a > b) zero
      else combine(f(a), mapReduce(f, combine, zero)(a+1, b))
  }

  def gProduct(f: Int => Int)(a: Int, b: Int): Int = mapReduce(f, (x,y) => x*y, 1)(a, b)
}
